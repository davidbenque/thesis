# Case Board, Traces, & Chicanes
### Diagrams for an archaeology of algorithmic prediction through critical design practice.

PhD Thesis in progress (submitted for examination on 29.01.2020)

Practice-based research by David Benqué at the [School of Communication](https://www.rca.ac.uk/schools/school-of-communication/communication_research/), Royal College of Art, London UK. This research is supported by Microsoft Research Cambridge (UK) as part of their PhD scholarship programme.  

Supervisors: Prof. Teal Triggs (RCA), Richard Banks (Microsoft Research)

## Abstract

This PhD thesis utilises diagrams as a language for research and design
practice to critically investigate algorithmic prediction. As a tool for
practice-based research, the language of diagrams is presented as a way
to *read* algorithmic prediction as a set of intricate computational
geometries, and to *write* it through critical practice immersed in the
very materials in question: data and code. From a position rooted in
graphic and interaction design, the research uses diagrams to gain
purchase on algorithmic prediction, making it available for examination,
experimentation, and critique. The project is framed by media
archaeology, used here as a methodology through which both the technical
and historical "depths" of algorithmic systems are excavated.

My main research question asks:

How can diagrams be used as a language to critically investigate
algorithmic prediction through design practice?

This thesis presents two secondary questions for critical examination,
asking:

Through which mechanisms does thinking/writing/designing in diagrammatic
terms inform research and practice focused on algorithmic prediction?

As algorithmic systems claim to produce objective knowledge, how can
diagrams be used as instruments for speculative and/or conjectural
knowledge production?

I contextualise my research by establishing three registers of relations
between diagrams and algorithmic prediction. These are identified as:
*Data Diagrams* to describe the algorithmic forms and processes through
which data are turned into predictions; *Control Diagrams* to afford
critical perspectives on algorithmic prediction, framing the latter as
an apparatus of prescription and control; and *Speculative Diagrams* to
open up opportunities for reclaiming the generative potential of
computation. These categories form the scaffolding for the three
practice-oriented chapters where I evidence a range of meaningful ways
to investigate algorithmic prediction through diagrams.

This includes, the 'case board' where I unpack some of the historical
genealogies of algorithmic prediction. A purpose-built graph application
materialises broader reflections about how such genealogies might be
conceptualised, and facilitates a visual and subjective mode of
knowledge production. I then move to producing 'traces', namely probing
the output of an algorithmic prediction system—in this case YouTube
recommendations. Traces, and the purpose-built instruments used to
visualise them, interrogate both the mechanisms of algorithmic capture
and claims to make these mechanisms transparent through data
visualisations. Finally, I produce algorithmic predictions and examine
the diagrammatic "tricks," or 'chicanes', that this involves. I revisit
a historical prototype for algorithmic prediction, the almanac
publication, and use it to question the boundaries between data-science
and divination. This is materialised through a new version of the
almanac—an automated publication where algorithmic processes are used
to produce divinatory predictions.

My original contribution to knowledge is an approach to practice-based
research which draws from media archaeology and focuses on diagrams to
investigate algorithmic prediction through design practice. I
demonstrate to researchers and practitioners with interests in
algorithmic systems, prediction, and/or speculation, that diagrams can
be used as a language to engage critically with these themes.

## Installation

This thesis is written in Visual Studio Code using the [LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) and [LaTeX Utilities](https://marketplace.visualstudio.com/items?itemName=tecosaur.latex-utilities) extensions. 

If using this setup the thesis compiles to PDF with the default recipes:  
`latexmk` and `pdflatex → bibtex → pdflatex → pdflatex`


