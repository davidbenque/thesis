import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#%%

iris = pd.read_csv("iris_sample.csv")
iris['class'] = iris['class'].astype("category")

species = iris['class'].unique()

print(iris.values)
# %%

# color according to species?
for index, row in iris.iterrows():
    plt.quiver(*row.values[0:4])

plt.savefig('iris_vectors.svg')
