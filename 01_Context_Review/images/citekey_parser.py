import re

file = open('../main.md')

match_citekey = '@[a-z0-9\.]*'
parts = {}

for line in file.readlines():
    if line[:3] == '## ':
        current_part = line[3:].strip()
        parts[current_part] = []

    else:
        citekeys = re.findall(match_citekey, line)
        for key in citekeys:
            if key not in parts[current_part]:
                parts[current_part].append(key)

parts
