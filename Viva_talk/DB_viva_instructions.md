David Benqué  
PhD Candidate  
School of Communication  
Royal College of Art



## Remote viva-voce presentation

Screen sharing will be used to present. The websites and browser suggested below can be used to review the practice work prior to May 4th 2020, and optionally to interact with the work on the day of the examination.

### Diagrams of the Future

Visit http://dotf.xyz/ in a web browser  
and https://gitlab.com/davidbenque/diagrams-of-the-future for the code repository



### Architectures of Choice Vol.1 YouTube

Visit https://gitlab.com/davidbenque/arc-choice for the code repository. 

sample animation preview: https://gitlab.com/davidbenque/arc-choice/-/raw/master/03-YT-traces/web_traces/screencast_trace.mp4

I will be replicating the peer-to-peer broadcast of the visualisation on the day of the exam. I will show it on my screen but if you would like to take part in the peer-to-peer network you can prepare by: 

- Installing the Beaker Browser: https://beakerbrowser.com/

On the day of the exam: 

- Visit [dat://yt-trace.davidbenque.com](dat://yt-trace.davidbenque.com) in Beaker Browser, if this address does not work, please use [dat://fdb7616d525e265bf05939ca91c75cc8e8112fd61f03e20d79aa132bb6279032](dat://fdb7616d525e265bf05939ca91c75cc8e8112fd61f03e20d79aa132bb6279032)
- Turn on "⚡ live reloading" in the `...` menu in the address bar.  



### Almanac.computer

Visit https://almanac.computer/ in a web browser. 
The Chromium browser is recommended for the print preview, with the following settings:

- page size: A5
- Headers and footers: disabled
- background graphics: enabled

Visit https://gitlab.com/davidbenque/almanac.computer for the code repository. 

The data for all issues are archived in the repository: https://gitlab.com/davidbenque/almanac.computer/-/tree/master/_archive 

