#!/bin/bash

# watch for changes to the markdown file and trigger this build script with:
# ls *.md | entr ./build.sh

# remove each slide as a page in history
# -V progress=false

pandoc DB_viva_talk.md \
-t revealjs -s \
-o DB_viva_talk.html \
--filter=/usr/bin/pandoc-citeproc \
--metadata pagetitle="DB Viva" \
--bibliography /home/david/Dropbox/_Library/Zotero.bib \
--csl /home/david/Dropbox/_Library/harvard-cite-them-right-SLIDE.csl \
-c style.css \
-V revealjs-url=/home/david/apps/reveal.js \
-V controls=false \
-V theme=white \
-V transition=none \
-V slideNumber=\"c/t\" \
-V backgroundTransition=none \
-V autoPlayMedia=true
