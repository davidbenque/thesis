# Case Board, Traces & Chicanes  

Diagrams for an archaeology of algorithmic prediction through critical design practice 

::: sub-title

David Benqué — Viva Voce  
May 4th 2020  

:::

::: notes

Thank you for agreeing to examine my thesis  
and for agreeing to these unusual exam conditions. 

In this presentation, I will very briefly summarise the research journey, culminating in my set of research questions, methods, and contributions.  

I will then summarise the three practice projects and my usage of diagrams in each of them. I will draw out specifically the type of archaeological practice that each of them affords, considering each one as a ==way of making & speculating==  critically about algorithmic prediction. 

Finally, I will open things up to future work. 

:::

---

![](DB_viva_talk.assets/simple_plan.svg){.stretch}

::: notes

My research looks at algorithmic prediction from a situation in design (graphic and interactive)

My initial proposal was centred on aesthetics, asking: How does prediction manifest itself through interfaces and how do these visual languages shape an image or narrative around prediction? 

:::

---

![](DB_viva_talk.assets/x-0009.jpg){.stretch}

::: ref

Slide from: Fields of Communication - 30.10.2018 - Royal College of Art 

:::

::: notes

This initial hunch changed through the research. I discovered that what had outlined was in fact a special case of a much deeper mediation of algorithmic prediction through *diagrams*. 

This key word oriented my research and helped me process and orient the practice work in two main directions: 

- it connected at one end to the *operational core* of algorithmic prediction: the materials I was manipulating: data, code, and in particular with the operations of machine learning. 
- At the other end it highlighted the political and cultural *ramifications* of algorithmic prediction, as found in the critical literature on algorithmic prediction, as well as on modes of making/knowing.

I have largely Adrian Mackenzie to thank for this shift towards diagrams as the timely publication of his book in the winter of 2017 was a key turning point in my PhD project. It then oriented my reading and re-reading of both the literature and my own practice-based research projects. 

Mackenzie's work is informed by the archaeology of knowledge, however it was only later when reading up on media-archaeology that this resonated with my practice-based research approach. This resulted in a position where I am ==taking/borrowing ideas from media-archaeology and moving them through my critical design practice== 

This culminated in the following set of questions: 

:::

---

## Research Questions 

---

**RQ**  
How can diagrams be used as a language to critically investigate algorithmic prediction through design practice?

::: notes

my main research question [read]

This unpacks into two secondary questions 

:::

---

**RQ1**  
Through which mechanisms does thinking/writing/designing in diagrammatic terms inform research and practice focused on algorithmic prediction?

::: notes

the first one [read]

positions diagrams between research and practice

:::

---

**RQ2**  
As algorithmic systems claim to produce objective knowledge, how can diagrams be used as instruments for speculative and/or conjectural knowledge production?

::: notes

and the second [read]

 centers the criticality of the research as an interrogation of how algorithmic prediction produces knowledge. Opposing abduction to the induction and deduction of algorithmic inference. 

:::



---

## Methods: 

instruments vs. instrumentalising



::: notes

to address these questions, I have detailed the following methods in the thesis: 

- Instrumented research 
- Speculative/abductive practice
- Publishing 

What I would like to draw out from these is a tension between using computational instruments while pushing against the instrumentalisation of knowledge production, and the instrumentalisation of design as part of a positivist imaginary of AI/prediction. 

This ran through my research as I immersed myself in materials and practices of data and prediction while seeking to not "surrender" my methods to them. (Nowviskie)

I addressed this by attending to practice based research projects as trajectories:  
Parisi draws a definition of speculative methods from Whitehead: like the flight of an airplane that departs from the ground of intuitions and hunches, makes a flight, and returns to be examined. 

More specifically for the computational context I was working in: 
Drucker & Novwiskie's speculative computing  
as well Fuller's evocative image of computation as a medium on which crystals can be grown  



:::



---

## Contributions

- Diagrams as a language to investigate algorithmic prediction through design practice. 

    - Conducive to an archaeological approach. 
    - Abductive/Speculative.

    


::: notes

My main contribution to knowledge is: the use of diagrams as a language for design to gain critical purchase on algorithmic prediction. 

This is evidenced in three different ways by the practice chapters.

This language is conducive to an archaeological approach that is immersed in the materials in question while attentive to their history and critical of dominant imaginaries of data and prediction.  

The critical element in my research is summarised in my distinction/refusal of algorithmic induction and empirical deduction as modes of knowledge production. Instead I position diagrams as a language that supports an abductive/speculative approach to practice-based research. 
:::

---

- Novel use for graph databases.
- Method for animating and broadcasting data visualisations over a peer-to-peer network. 
- Method for automated production of almanac publications.

::: notes

Through this work I have also made a number of secondary contributions of a more practical nature, namely [read]

I will now very briefly touch on each of the practice chapters.

:::



---

## 1. Case Board

![](DB_viva_talk.assets/Screenshot from 2019-11-18 21-19-39.png){.stretch}

Diagrams of the Future - http://dotf.xyz

::: notes

Diagrams of the future: an archive of predictive diagrams, itself in the form of a diagram. 

demonstrates the use of diagrams to excavate the history of algorithmic prediction, a way of *remembering* how we got to the present moment. 


:::

---

## Sleuthing

![](DB_viva_talk.assets/tumblr_o16n2kBlpX1ta3qyvo1_1280.jpg){.stretch}

::: ref

slide from: Sonder les dispositifs numériques - 18.10.2019

:::

::: notes

my research here is in setting the stage for a form of ==sleuthing==: a piecing together of the genealogies of algorithmic prediction. Working towards the resolution of a plot that never fully adds up to a resolved crime. 

As Mackay highlights, it is a performance of knowledge production

>  a diagram of a diagram of thought in action

that allows research to progress along lines, threads, and relations. 

On the other hand this practice comes with an accumulation of paperwork (Toscano & Kinkle on *The Wire*) and is under constant threat from tedium and the personal preoccupations of the researcher. This may even descend into 'sheer delirium' (Mackay). 

@mackay2017
:::

---

## 2. Traces

![](DB_viva_talk.assets/trace-web.png){.stretch}

 Architectures of Choice Vol. 1: YouTube

::: notes 

Architectures of Choice Vol. 1 Youtube: demonstrates the use of diagram to probe algorithmic prediction, or rather its output. 

I move up-close to one specific engine of prediction, namely the recommendations on YouTube.  

Here diagrams are a means to *observe* algorithmic prediction through a form of probing

:::

---

## Probing

![](DB_viva_talk.assets/Injection Lorette.jpg){.stretch}

::: ref

Slide from: Speculative Interfaces - IMPAKT Festival - 30.10.2019

:::

::: notes

==probing==: the release of agents or probes to generate *traces* from the outputs of predictive algorithms. 

This type of work is at once critical of, and 'hosted' (as Seaver puts it) by algorithmic systems and their logics of entrapment. 

Probing is about finding ways to access systems that do not want to be studied, but also about reclaiming the interpretation of traces as a conjectural mode of knowledge production, a way to 'imagine the appearance of an algorithmic system we will never see'  (to paraphrase Carlo Ginzburg)

>  from the barest traces they were able to reconstruct the appearance of an
> animal they'd never set eyes on

@ginzburg1980

:::

---


## 3. Chicanes

![](DB_viva_talk.assets/image-20200422105718887.png){.stretch}

almanac.computer

::: notes

Almanac.computer (or Monistic Almanac) demonstrates the use of diagrams as instruments for divination. 

Specifically, I put the data diagrams of algorithmic prediction to work for astrological rationalities. 

This is staged in the specific context of the almanac publication, that I  re-*imagined* for the current moment of algorithmic prediction. 


:::

---

## Divining

![IMG_4212](DB_viva_talk.assets/IMG_4212.jpg){.stretch}

::: ref

Image from: Supra Systems Office Rites - Victoria & Albert Museum, Digital Design weekend Oct. 2018

:::

::: notes

==divining==: producing predictions as a way of paying attention to the mediation of our relationship to chance (Ramey). 

Taking divination seriously means considering every relationship to chance as mediated. 

The tricks, or chicanes, of divination are not played by the diviner on the client, but rather form a process where both produce something together. 

The positivist imaginary of data insists that there exists one stable truth, and that data gives unmediated access to it.

re-framing algorithmic prediction as a mode of divination challenges this view, and opens the door to a multiplicity of other forms of knowledge production about the future. 

Through this it is a way of questioning, as Stengers puts it, the *credentials* assigned, or not, to predictions.



:::



---


![](DB_viva_talk.assets/activities.svg){.stretch}

::: notes

What I wanted to draw out here is how each diagram type supports a type of archaeological research practice. 

These are the activities, or personas, that I have taken on in turn during the research. As I discussed in the thesis, these were not conducted in isolation from each other but fed into each other throughout the research. 

To conclude I want to emphasise that my contribution is not a generalisation of these three cases. Instead, my intent is to demonstrate how diagrams afford different types of purchase on algorithmic prediction, and to encourage the formation of other generative combinations, rather than scientific replication. 

with this I turn to future work

:::

---

## Future Work

![](DB_viva_talk.assets/LOGO.svg){.stretch}

::: notes

I plan to continue this work, for this I have started setting up my practice as the Institute of Diagram Studies. This will be used for: 

- maintenance and development of the projects I have presented

- new projects such as publications and collaborations

- providing community services: for example Diagramstagram, an open source federated alternative to Instagram where only diagrams are allowed. 

and generally to expand on the work I outlined here. Looking at the world, and specifically at systems involving data and algorithms, through the lens of diagrams. 

- vectoral politics
- computational geometries 
- 

:::

---

FIN