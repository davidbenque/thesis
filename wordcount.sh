#! /bin/bash

w="Words in text: "
count_result=$(texcount -inc -total  main.tex | grep "$w")
count=${count_result#"$w"}
count_format=$(echo $count | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')
echo "Word count: $count_format" | tee wordcount.tex